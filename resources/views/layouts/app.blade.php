<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css') }}
    {{ HTML::script('//code.jquery.com/jquery-1.10.1.min.js') }}
    {{ HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') }}

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        @php
            $nav_array = array();
            $nav_array[] = array('link'=>'/','title'=>'Home');
            if (Auth::guest()) {
                $nav_array[] = array('link'=>'/login','title'=>'Login');
                $nav_array[] = array('link'=>'/register','title'=>'Register');
            } else {
                $nav_array[] = array(Auth::user()->name, array(
                    array('link'=>'#','title'=>Auth::user()->name),
                    array('link'=>'/logout','title'=>'Log out'),
                ));

            }
        @endphp
        {!!
            Navbar::withBrand( config('app.name', 'Laravel'),'/')
                ->withContent(Navigation::links($nav_array)->right())
        !!}

        @yield('content')
    </div>
</body>
</html>
